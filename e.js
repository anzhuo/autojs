//交流QQ群:202391358  作者:shituo
//======================以下类用于base64编码解码======================
importClass(android.util.Base64);//引用安卓类库
//======================以下类用于签名校验======================
importClass(android.content.pm.PackageManager);
importClass(java.security.MessageDigest);
importClass(java.io.ByteArrayInputStream);
importClass(java.lang.StringBuilder);
importClass(java.lang.Integer);
importClass(java.security.cert.CertificateFactory);
//=====================================================



let m = ["调试输出",
    "时间_取当前时间戳", "时间_取网络时间戳", "时间_取网络时间", "时间_时间戳转时间", "时间_时间转文本", "时间_文本转时间",
    "系统_取本机外网IP",
    "编码_十六转base64", "编码_base64转十六", "编码_base64转字节数组", "编码_字节数组转base64", "编码_整数转字符", "编码_文本转字节数组", "编码_字节数组转文本", "编码_十六进制转字节数组", "编码_字节数组转十六进制", "编码_字节数组转十六进制2", "编码_UTF8解码",
    "编码_UTF8编码",
    "文本_取中间文本",
    "日志_写日志行",
    "算法_UUID", "取随机数",
    "应用_取版本", "应用_取版本号", "应用_取签名", "应用_取名称", "应用_是否安装", "应用_取版本号", "应用_取类名", "应用_取已安装列表",
    "校验_取CRC32",
];
var e = {};
/*
//print('交流QQ群:202391358');
Array.prototype.intersect = function () {
    var result = new Array();
    var obj = {};
    for (var i = 0; i < arguments.length; i++) {
        for (var j = 0; j < arguments[i].length; j++) {
            var str = arguments[i][j];
            if (!obj[str]) {
                obj[str] = 1;
            } else {
                obj[str]++;
                if (obj[str] == arguments.length) {
                    result.push(str);
                }
            } //end else
        } //end for j
    } //end for i
    return result;
}
//集合去掉重复
Array.prototype.uniquelize = function () {
    var tmp = {},
        ret = [];
    for (var i = 0, j = this.length; i < j; i++) {
        if (!tmp[this[i]]) {
            tmp[this[i]] = 1;
            ret.push(this[i]);
        }
    }
    return ret;
}
//并集
Array.prototype.union = function () {
    var arr = new Array();
    var obj = {};
    for (var i = 0; i < arguments.length; i++) {
        for (var j = 0; j < arguments[i].length; j++) {
            var str = arguments[i][j];
            if (!obj[str]) {
                obj[str] = 1;
                arr.push(str);
            }
        } //end for j
    } //end for i
    return arr;
}*/
e.调试输出 = function (输出数据) {
    print(输出数据);
}
//====================时间============================时间================================时间==========================
e.时间_取当前时间戳 = function () {
    var timestamp = new Date().getTime();
    return timestamp;//加8小时的时区
}

e.时间_取网络时间戳 = function () {
    var gettime = http.get('http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp');
    var time = gettime.body.json();
    return parseInt(time.data.t);
}
e.时间_取网络时间 = function () {
    var gettime = http.get('http://quan.suning.com/getSysTime.do');
    var time = gettime.body.json();
    return time.sysTime2;
}

e.时间_时间戳转时间 = function (time) {
    if (typeof (time) == "string") {
        return new Date(parseInt(time));
    }
    return new Date(time);
}

e.时间_时间转文本 = function (时间, 取日期or时间部分, 是否带毫秒) {
    if (!arguments[0]) 时间 = new Date();//给第三个参数设置默认值
    //var 时间 = new Date(date);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = 时间.getFullYear();
    var M = 时间.getMonth() + 1;
    M = M < 10 ? '0' + M : M;
    var D = 时间.getDate();
    D = D < 10 ? '0' + D : D;
    var h = 时间.getHours();
    h = h < 10 ? '0' + h : h;
    var m = 时间.getMinutes();
    m = m < 10 ? '0' + m : m;
    var s = 时间.getSeconds();
    s = s < 10 ? '0' + s : s;
    if (取日期or时间部分 == 1) {
        return Y + "-" + M + "-" + D;
    } else if (取日期or时间部分 == 2) {
        return h + ":" + m + ":" + s;

    } else {
        if (是否带毫秒) {
            var ms = 时间.getMilliseconds();
            //log(ms);
            return Y + "-" + M + "-" + D + " " + h + ":" + m + ":" + s + "." + ms;
        }
        return Y + "-" + M + "-" + D + " " + h + ":" + m + ":" + s;
    }
}
e.时间_文本转时间 = function (date_str) {
    return new Date(date_str.replace(" ", "T"));
}
//====================系统============================系统================================系统==========================
e.系统_取本机外网IP = function () {
    var getIp_api = http.get('http://apps.game.qq.com/comm-htdocs/ip/get_ip.php');
    var InetIP = getIp_api.body.json();
    return InetIP.ip_address;
}
//==================日志=========================日志==============================
e.日志_写日志行 = function (日志路径, 日志文本, 级别) {
    if (!arguments[2]) 级别 = 0;//给第三个参数设置默认值
    let 日志级别 = ['信息', '警告', '错误', '调试'];//0=信息，1=警告，2=错误，3=调试
    日志文本 = e.时间_时间转文本(null, 0, false) + " " + 日志级别[级别] + ":" + 日志文本 + "\n"
    files.append(日志路径, 日志文本);
    log(日志文本);
}
//====================编码转换=========================编码转换==========================================================

e.编码_整数转字符 = function (n) {
    var BI_RM = "0123456789abcdefghijklmnopqrstuvwxyz";
    return BI_RM.charAt(n);
}
e.编码_十六转base64 = function (h) {
    var b64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var b64pad = "=";
    var i;
    var c;
    var ret = "";
    for (i = 0; i + 3 <= h.length; i += 3) {
        c = parseInt(h.substring(i, i + 3), 16);
        ret += b64map.charAt(c >> 6) + b64map.charAt(c & 63);
    }
    if (i + 1 == h.length) {
        c = parseInt(h.substring(i, i + 1), 16);
        ret += b64map.charAt(c << 2);
    }
    else if (i + 2 == h.length) {
        c = parseInt(h.substring(i, i + 2), 16);
        ret += b64map.charAt(c >> 2) + b64map.charAt((c & 3) << 4);
    }
    while ((ret.length & 3) > 0) {
        ret += b64pad;
    }
    return ret;
}
e.编码_base64转十六 = function (s) {
    var b64map = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var b64pad = "=";
    var ret = "";
    var i;
    var k = 0; // b64 state, 0-3
    var slop = 0;
    for (i = 0; i < s.length; ++i) {
        if (s.charAt(i) == b64pad) {
            break;
        }
        var v = b64map.indexOf(s.charAt(i));
        if (v < 0) {
            continue;
        }
        if (k == 0) {
            ret += e.编码_整数转字符(v >> 2);
            slop = v & 3;
            k = 1;
        }
        else if (k == 1) {
            ret += e.编码_整数转字符((slop << 2) | (v >> 4));
            slop = v & 0xf;
            k = 2;
        }
        else if (k == 2) {
            ret += e.编码_整数转字符(slop);
            ret += e.编码_整数转字符(v >> 2);
            slop = v & 3;
            k = 3;
        }
        else {
            ret += e.编码_整数转字符((slop << 2) | (v >> 4));
            ret += e.编码_整数转字符(v & 0xf);
            k = 0;
        }
    }
    if (k == 1) {
        ret += e.编码_整数转字符(slop << 2);
    }
    return ret;
}

e.编码_base64转字节数组 = function (base64) {
    return Base64.decode(base64, Base64.NO_WRAP);
}

e.编码_字节数组转base64 = function (base64arr) {
    return Base64.encodeToString(base64arr, Base64.NO_WRAP);
}
e.编码_字节数组转十六进制2 = function (arr, 分割符) {
    let str = new StringBuilder(arr.length * 2);
    for (let i = 0; i < arr.length; i++) {
        let h = Integer.toHexString(arr[i]);
        let l = h.length;
        if (l == 1) h = "0" + h;
        if (l > 2) h = h.substring(l - 2, l);
        str.append(h.toUpperCase());
        if (分割符 == null) 分割符 = ":";
        if (i < arr.length - 1) str.append(分割符);
    }
    return str.toString();
}

e.编码_文本转字节数组 = function (string) {
    var bytes = [];
    for (var i = 0; i < string.length; i++) {
        bytes.push(string.charCodeAt(i));
    }
    return bytes;
}
e.编码_字节数组转文本 = function (bytes) {
    var string = "";
    for (var i = 0; i < bytes.length; i++) {
        string += String.fromCharCode(bytes[i]);
    }
    return string;
}
e.编码_十六进制转字节数组 = function (str) {
    var pos = 0;
    var len = str.length;
    if (len % 2 != 0) {
        return null;
    }
    len /= 2;
    var hexA = [];
    for (var i = 0; i < len; i++) {
        var s = str.substr(pos, 2);
        var v = parseInt(s, 16);
        hexA.push(v);
        pos += 2;
    }
    return hexA;
}
e.编码_字节数组转十六进制 = function (arrBytes) {
    var str = ""
    for (var i = 0; i < arrBytes.length; i++) {
        var tmp;
        var num = arrBytes[i];
        if (num < 0) {
            //此处填坑，当byte因为符合位导致数值为负时候，需要对数据进行处理
            tmp = (255 + num + 1).toString(16);
        } else {
            tmp = num.toString(16);
        }
        if (tmp.length == 1) {
            tmp = "0" + tmp;
        } else {
            str += tmp;
        }
    }
    return str;
}

e.编码_UTF8编码 = function (str) {
    var result = [];
    var k = 0;
    for (var i = 0; i < str.length; i++) {
        var j = encodeURI(str[i]);
        if (j.length == 1) {
            // 未转换的字符
            result[k++] = j.charCodeAt(0);
        } else {
            // 转换成%XX形式的字符
            var bytes = j.split("%");
            for (var l = 1; l < bytes.length; l++) {
                result[k++] = parseInt("0x" + bytes[l]);
            }
        }
    }
    return result;
}

e.编码_UTF8解码 = function (arr) {
    let val = ''
    arr.forEach(item => {
        if (item < 127) {
            val += String.fromCharCode(item)
        } else {
            val += '%' + item.toString(16).toUpperCase()
        }
    })
    console.log(val)
    try {
        return decodeURI(val)
    } catch (err) {
        return val
    }
}
//=============================文本========================文本=========================================
e.文本_取中间文本 = function (待取文本, 开始文本, 结束文本) {
    let 开始位置 = 待取文本.indexOf(开始文本);
    //print(开始位置, 开始文本.length);
    if (开始位置 > -1) {
        let 结束位置 = 待取文本.indexOf(结束文本, 开始位置 + 开始文本.length);
        //print(结束位置)
        if (结束位置 > -1) {
            return 待取文本.substring(开始位置 + 开始文本.length, 结束位置);
        }
    }
    return '';
}
//==========================算术计算==============核心程序=============================================
e.取随机数 = function (min, max) {
    if (min == 0) {
        return parseInt(Math.random() * (max + 1), 10);
    }
    return parseInt(Math.random() * (max - min + 1) + min, 10);
}
e.算法_UUID = function () {
    let hexDigits = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"];
    //hexDigits = hexDigits.split("");
    let s = new Array(36); let d;
    for (let i = 0; i < 36; i++) {
        let r = e.取随机数(0, 15);
        //log(i);
        if (i == 19) { d = r; }
        s[i] = hexDigits[r];
    }
    s[14] = "4";
    s[19] = hexDigits[d & 3 | 8];
    s[8] = "-"; s[13] = "-"; s[18] = "-"; s[23] = "-";
    return s.join("");
}
//============================================校验=========校验==================================================
e.校验_取CRC32 = function (/* String */ str, /* Number */ crc) {//该函数只处理ASCII字符，如需处理中文，需要先编码
    var table = "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D";
    /* Number */
    if (crc == undefined) crc = 0;
    var n = 0; //a number between 0 and 255
    var x = 0; //an hex number
    crc = crc ^ (-1);
    for (var i = 0, iTop = str.length; i < iTop; i++) {
        n = (crc ^ str.charCodeAt(i)) & 0xFF;
        x = "0x" + table.substr(n * 9, 8);
        crc = (crc >>> 8) ^ x;
    }
    crc = crc ^ (-1)
    return ((crc ^ (-1)) >>> 0).toString(16);
}
//============================================应用=========应用==================================================
e.应用_取签名 = function () {
    //获取包管理器
    let pm = context.getPackageManager();

    //获取当前要获取 SHA1 值的包名，也可以用其他的包名，但需要注意，
    //在用其他包名的前提是，此方法传递的参数 Context 应该是对应包的上下文。
    let packageName = context.getPackageName();

    //返回包括在包中的签名信息
    let flags = PackageManager.GET_SIGNATURES;

    //获得包的所有内容信息类
    let packageInfo = pm.getPackageInfo(packageName, flags);

    //签名信息
    let signatures = packageInfo.signatures;
    let cert = signatures[0].toByteArray();

    //将签名转换为字节数组流
    let input = new ByteArrayInputStream(cert);

    //证书工厂类，这个类实现了出厂合格证算法的功能
    let cf = CertificateFactory.getInstance("X509");

    //X509 证书，X.509 是一种非常通用的证书格式
    let c = cf.generateCertificate(input);

    //加密算法的类，这里的参数可以使 MD4, MD5 ,SHA,SHA1,SHA-1 等加密算法
    let md = MessageDigest.getInstance("SHA1");

    //获得公钥
    let publicKey = md.digest(c.getEncoded());

    //字节到十六进制的格式转换
    let hexString = this.编码_字节数组转十六进制(publicKey);
    return hexString;
}


e.应用_取版本2 = function (应用名称) {//速度稍微慢点 
    let package_name = app.getPackageName(应用名称);
    let pkgs = context.getPackageManager().getInstalledPackages(0).toArray();
    for (let i in pkgs) {
        if (pkgs[i].packageName) {
            return pkgs[i].versionName;
        }
    }
    return '';
}

e.应用_是否安装 = function (应用名称) {//速度稍微慢点 
    let package_name = app.getPackageName(应用名称);
    // let pkgs = context.getPackageManager().getPackageInfo(package_name, 0);
    if (package_name != null) {
        return true;
    }
    return false;
}

e.应用_取已安装列表 = function () {
    let pkgs = context.getPackageManager().getInstalledPackages(0).toArray();
    let apps = new Array();
    for (let i in pkgs) {
        apps[i] = this.应用_取名称(pkgs[i].packageName);
    }
    return apps;
}

e.应用_取版本 = function (package_name) {
    if (package_name == null) {
        return "应用包名不能为空!";
    }
    let pkgs = context.getPackageManager().getPackageInfo(package_name, 0);
    return pkgs.versionName;

    // String packageName	包名
    // String[] splitNames
    // int versionCode	版本号
    // String versionName	版本名称

    // ApplicationInfo applicationInfo
    /*ApplicationInfo描述应用程序的基本信息
    public String taskAffinity
    public String permission	权限
    public String processName	名字
    public String className	类名
    public int descriptionRes
    public static final int FLAG_SYSTEM = 1<<0	系统应用
    public static final int FLAG_EXTERNAL_STORAGE = 1<<18	安装在外部存储卡
    public static final int FLAG_LARGE_HEAP = 1<<20	支持大内存*/

    // long firstInstallTime	第一次安装时间
    // long lastUpdateTime	上次更新时间
    // ActivityInfo[] activities	所有的Activity信息
    // ActivityInfo[] receivers	所有的广播接收者
    // ServiceInfo[] services	所有的服务信息
    // ProviderInfo[] providers	获取ContentProvide
    // PermissionInfo[] permissions	所有的权限信息

}
e.应用_取版本号 = function (package_name) {
    if (package_name == null) {
        return "应用包名不能为空!";
    }
    let pkgs = context.getPackageManager().getPackageInfo(package_name, 0);
    return pkgs.versionCode;
}
e.应用_取类名 = function (package_name) {
    if (package_name == null) {
        return "应用包名不能为空!";
    }
    let pkgs = context.getPackageManager().getPackageInfo(package_name, 0);
    return pkgs.applicationInfo.className;
}
e.应用_取名称 = function (应用包名) {
    if (应用包名 == null) {
        return "应用包名不能为空!";
    }
    let pm = context.getPackageManager();
    let c = pm.getApplicationLabel(pm.getApplicationInfo(应用包名, PackageManager.GET_META_DATA))
    return c.toString();
}
module.exports = e;