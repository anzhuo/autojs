# auto.js中文模块e

#### 介绍
auto.js中文模块e 模块   交流QQ群:202391358
文档:https://gitee.com/anzhuo/e.js/wikis/%E9%A6%96%E9%A1%B5-%E4%BB%8B%E7%BB%8D

###  **autu.js 踩过的坑** 


 
1. RSA 加密后长度为64 文本太长了加密不了
2. RSA 加密不支持自定义构建 秘钥 只支持 用API生成的!
3. AES 加密 秘钥长度 最多16位(128bits)

#### 安装代码


```
function 安装AUTOJS中文模块() {// 回调法 加载模块
    var url = 'https://gitee.com/anzhuo/autojs/raw/master/e.js';
    http.get(url, {}, function (res, err) {
        if (err) {
            console.error(err);
            return;
        }
        log("下载成功 = " + (res.statusCode == '200'));
        var html源码 = res.body.string();
        //log("代码:" + 模块.body.string());
        files.write('./e.js', html源码, "utf-8");
        //files.writeBytes('./e.js', html源码);
        log("QQ群:202391358  www.autojs.cc 开源社区");
    });
    //return e;
}
安装AUTOJS中文模块();
var e = require('./e.js'); //引用 auto.js  E中文模块

```
